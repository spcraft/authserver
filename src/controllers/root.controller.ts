/* ---- IMPORTS ---- */
import { Router, Request, Response } from "express";
import { IAuthorization } from "../requests/IAuthorization";
import * as ldap from "ldapjs";
import * as config from "config";
import { sign } from "jsonwebtoken";

/* ---- LDAP SERVICE ---- */

const ldapClient: ldap.Client = function initLDAP() {
    return ldap.createClient({
        url: `${config.get("ldap.protocol")}://${config.get("ldap.host")}:${config.get("ldap.port")}`,
        reconnect: true,
    });
}();



/* --- CONTROLLER ---- */
const controller : Router = Router();
export default controller;

/**
 * Authenticates the user
 */
controller.post("/", function(req: Request, res: Response) {
    new Promise<IAuthorization>(function(resolve, reject) {
        /* ---- CAST THE REQUEST BODY INTO THE AUTH FORM ---- */
        try {
            resolve(req.body as IAuthorization);
        } catch(err) {
            reject(err);
        }
    }).then(function(authForm: IAuthorization) {
        /* ---- CHECK IF THE APPLICATION IS DEFINED IN CONFIG ---- */
        return new Promise<IAuthorization>(function(resolve, reject) {
            if(
                config.has(`applications.${authForm.application}.secret`)
                && config.has(`applications.${authForm.application}.loginUrl`)
            ) resolve(authForm);
            else reject(new Error("Application not found"));
        });
    }).then(function(authForm: IAuthorization) {
        /* ---- CHECK IF THE USER EXISTS IN THE LDAP DATABASE ---- */
        return new Promise<[IAuthorization, any]>(function(resolve, reject) {
            ldapClient.search(
                config.get("ldap.baseDN"),
                {
                    scope: "sub",
                    filter: `(&(objectClass=posixAccount)(objectClass=inetOrgPerson)(|(uid=${authForm.username})(mail=${authForm.username})))`
                },
                function(error, result) {
                    if(error) reject(error);
                    let isFound: boolean = false;
                    result.on("searchEntry", function(entry) {
                        if(isFound) return;
                        isFound = true;
                        resolve([authForm, entry.object]);
                    });
                    result.on("end", function() {
                        if(isFound) return;
                        reject(new Error("User was not found"));
                    });
                }
            );
        });
    }).then(function([authForm, userEntry]: [IAuthorization, any]) {
        /* ---- CHECK IF THE USER HAS ENTERED PROPER PASSWORD ---- */
        return new Promise<[IAuthorization, any]>(function(resolve, reject) {
            ldapClient.bind(
                userEntry.dn,
                authForm.password,
                function(error) {
                    if(error) reject(new Error("Incorrect combination of username and password"));
                    else {
                        ldapClient.unbind(function(err: ldap.Error) {
                            if(err) {
                                console.log(err);
                            }
                        });
                        resolve([authForm, userEntry]);
                    }
                }
            );
        });
    }).then(function([authForm, userEntry]: [IAuthorization, any]) {
        /* ---- RETURN THE AUTH TOKEN IN CASE ALL PREVIOUS STAGES ARE PASSED ---- */
        const token = sign(
            userEntry, 
            config.get(`applications.${authForm.application}.secret`),
            config.get("token.options")
        );

        if(req.accepts("html")) {
            res.redirect(
                `${ 
                    authForm.callbackUri ?
                    authForm.callbackUri :
                    config.get(`applications.${ 
                        authForm.application 
                    }.loginUrl`)
                }?token=${ token }`
            );
        } else res.json({
                user: userEntry,
                token: token,
        });
    }).catch(function(err: Error) {
        /* ---- RETURN AN ERROR IN CASE SOMETHING WENT WRONG ---- */
        res.json(err.message);
    });
});


