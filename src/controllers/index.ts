/* ---- IMPORTS ---- */
import { readdirSync, lstatSync } from "fs";
import { Application, Router } from "express";



/* --- FUNCTIONS ---- */

/**
 * Given the express application, sets the controllers for this app
 * @param app an express applications
 */
export function initControllers(app: Application) {
    readdirSync(__dirname).forEach((element: string) => {
        if(lstatSync(`${__dirname}/${element}`).isFile()) {
            const parts: string[] = element.split(".");
            if(parts[parts.length - 2] === "controller") {
                app.use(
                    `/${parts[0] === "root" ? "" : parts[0]}`,
                    require(`${__dirname}/${element.slice(0, element.length - 3)}`).default
                );
            }
        }
    })
}
