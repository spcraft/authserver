/* ---- IMPORTS ---- */
import express = require("express");
import config  = require("config");
import bodyParser = require("body-parser");
import { initControllers } from "./controllers";

/* ---- MAIN FUNCTION ---- */
(function main(): void {
    /* ---- OBJECTS ---- */
    const app: express.Application = express();



    /* ---- MIDDLEWARE ---- */
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded());



    /* ---- CONTROLLERS ---- */
    initControllers(app);
    app.use("/", express.static("./public"));

    
    /* ---- SERVER START ---- */
    app.listen(process.env.PORT || 3000);
    console.log(`The server is listening on port ${ process.env.PORT || 3000 }`);
})();

