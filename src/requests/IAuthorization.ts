/**
 * An authorization request
 * @author wize
 * @version 0 (6 Apr 2019)
 */
export interface IAuthorization {
    /** The LDAP username of the request sender */
    readonly username    : string;
    /** The LDAP password of the request sender */
    readonly password    : string;
    /** The application name, to which the access should be granted */
    readonly application : string;
    /** The redirect link after login route (not necessary) */
    readonly callbackUri?: string;
}